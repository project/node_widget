<?php

/**
 * Implementation of hook_node_widget_widgets_info()
 */
function optionwidgets_node_widget_widgets_info() {
  return array(
    'optionwidgets_select',
    'optionwidgets_buttons',
    'optionwidgets_onoff',
  );
}