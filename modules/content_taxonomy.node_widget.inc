<?php

/**
 * Implementation of hook_node_widget_widgets_info()
 */
function content_taxonomy_node_widget_widgets_info() {
  return array(
    'content_taxonomy_options',
    'content_taxonomy_select',
  );
}
